# Preview all emails at http://localhost:3000/rails/mailers/manage_user
class ManageUserPreview < ActionMailer::Preview
  def test_register_email
    ManageUser.register_mail(User.first)
  end

end
