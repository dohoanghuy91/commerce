class HomesController < ApplicationController
  before_action :authenticate_user!, only: [:show]

  def show

    render 'show', locals: {signed_in: user_signed_in? }
  end

  def index
    puts "=>>>>>>>>>>>>>>>>>>> HOME#INDEX <<<<<<<<<<<<<<<<<"
    puts current_user.inspect
    puts "=>>>>>>>>>>>>>>>>SESSION<<<<<<<<<<<<<<<<<<<<<<<<"
    puts user_session.inspect
    puts session.inspect
    render 'index', locals: {signed_in: user_signed_in?, session: session, user_session: user_session }
  end
end
