class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  #after_create :send_register_email

  def send_register_email
    @user = User.find self.id
    ManageUser.register_mail(@user).deliver
  end

end
