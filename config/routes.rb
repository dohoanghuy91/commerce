Rails.application.routes.draw do
  #devise_for :users
  root 'homes#index'
  get '/show' => 'homes#show'

  devise_for :users, controllers: { registrations: "users/registrations", sessions: 'users/sessions', passwords: 'users/passwords'}
  #devise_for :users, path: 'accounts'

end
